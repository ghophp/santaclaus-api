package adapter

import (
	"crypto/md5"
	"fmt"
	"net/http"

	"bitbucket.org/ghophp/santaclaus-api/session"

	redigo "github.com/garyburd/redigo/redis"
	"github.com/motain/samodelkin/redis"
)

const (
	// UserIdHeader is the name of the header from the request that is used to
	// store the user id, this allows the handlers to retrieve the id and find the
	// user if they need the entity
	UserIdHeader = "X-User-ID"

	// TenMinutes is ten minutes in seconds
	TenMinutes = 600
)

// Adapter is the struct to enable the decorator pattern
// 	http://www.alexedwards.net/blog/making-and-using-middleware
type Adapter func(http.HandlerFunc) http.HandlerFunc

// Adapt receives the basic HandlerFunc and decorate it with a list of Adapter
func Adapt(h http.HandlerFunc, adapters ...Adapter) http.HandlerFunc {
	for _, adapter := range adapters {
		h = adapter(h)
	}
	return h
}

// Cors is a decorator that allow the preflight for the handlers
func Cors() Adapter {
	return func(h http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
			w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

			if r.Method == "OPTIONS" {
				return
			}

			h(w, r)
		}
	}
}

// Auth is a decorator that allows the authentication process out of the box for the handlers
// as a result, either return the StatusUnauthorized response of set the request header UserIdHeader
// it is important to observe that it uses redis to cache the session for 10 minutes, avoiding the
// server to request facebook verification all the time
func Auth(redisConnector redis.Connector, sessionManager session.SessionManager) Adapter {
	return func(h http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			authToken := r.Header.Get("Authorization")
			if len(authToken) <= 0 {
				http.Error(w, "invalid Authorization header", http.StatusUnauthorized)
				return
			}

			rConn := redisConnector.Connect()
			defer rConn.Close()

			hash := fmt.Sprintf("%x", md5.Sum([]byte(authToken)))

			id, err := redigo.String(rConn.Do("GET", hash))
			if err != nil || len(id) <= 0 {
				id, err = sessionManager.GetUserDetails(authToken)
				if err != nil {
					http.Error(w, err.Error(), http.StatusUnauthorized)
					return
				}

				// set 2 hours cache for tokens, it is the same expiration time as facebook have
				rConn.Do("SET", hash, id)
				rConn.Do("EXPIRE", hash, TenMinutes)
			}

			r.Header.Set(UserIdHeader, id)
			h(w, r)
		}
	}
}
