package adapter

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/garyburd/redigo/redis"
	"github.com/rafaeljusto/redigomock"
	gc "gopkg.in/check.v1"
)

var _ = gc.Suite(&AdapterSuite{})

type (
	AdapterSuite struct{}

	MockSessionManager struct{}

	MockRedisConnector struct {
		rConn redis.Conn
	}
)

func (m *MockSessionManager) GetUserDetails(token string) (string, error) {
	return "testID", nil
}

func NewMockRedisConnector(rConn redis.Conn) *MockRedisConnector {
	return &MockRedisConnector{rConn}
}

func (r *MockRedisConnector) Connect() redis.Conn {
	return r.rConn
}

func (r *MockRedisConnector) PingConnect() (redis.Conn, error) {
	return r.rConn, nil
}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { gc.TestingT(t) }

func (h *AdapterSuite) TestCorsAdapter(c *gc.C) {
	count := 0
	adapted := Adapt(func(w http.ResponseWriter, r *http.Request) {
		count = count + 1
	}, Cors())

	req, err := http.NewRequest(
		"OPTIONS",
		"",
		nil,
	)
	c.Check(err, gc.IsNil)

	w := httptest.NewRecorder()
	adapted(w, req)

	c.Check(count, gc.Equals, 0)
	c.Check(w.Header().Get("Access-Control-Allow-Origin"), gc.Equals, "*")
	c.Check(w.Header().Get("Access-Control-Allow-Methods"), gc.Equals, "POST, GET, OPTIONS, PUT, DELETE")
	c.Check(w.Header().Get("Access-Control-Allow-Headers"), gc.Equals, "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	req, err = http.NewRequest(
		"GET",
		"",
		nil,
	)
	c.Check(err, gc.IsNil)

	w = httptest.NewRecorder()
	adapted(w, req)

	c.Check(count, gc.Equals, 1)
}

func (h *AdapterSuite) TestAuthAdapterInvalidHeader(c *gc.C) {
	mockConn := redigomock.NewConn()
	mockRedisConnector := NewMockRedisConnector(mockConn)

	adapted := Adapt(
		func(w http.ResponseWriter, r *http.Request) {},
		Auth(mockRedisConnector, &MockSessionManager{}),
	)

	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	w := httptest.NewRecorder()
	adapted(w, req)

	c.Check(string(w.Body.Bytes()), gc.Equals, "invalid Authorization header\n")
	c.Check(w.Code, gc.Equals, http.StatusUnauthorized)
}

func (h *AdapterSuite) TestAuthAdapterFromSessionStorage(c *gc.C) {
	testAuthToken := "testauthtoken"
	testId := "testID"
	testHash := fmt.Sprintf("%x", md5.Sum([]byte(testAuthToken)))

	mockConn := redigomock.NewConn()
	mockConn.Command("GET", testHash).Expect(testId)

	mockRedisConnector := NewMockRedisConnector(mockConn)
	adapted := Adapt(
		func(w http.ResponseWriter, r *http.Request) {},
		Auth(mockRedisConnector, &MockSessionManager{}),
	)

	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set("Authorization", testAuthToken)

	w := httptest.NewRecorder()
	adapted(w, req)

	c.Check(req.Header.Get(UserIdHeader), gc.Equals, testId)
}

func (h *AdapterSuite) TestAuthAdapterFromFacebook(c *gc.C) {
	testAuthToken := "testauthtoken"
	testId := "testID"
	testHash := fmt.Sprintf("%x", md5.Sum([]byte(testAuthToken)))

	mockConn := redigomock.NewConn()
	mockConn.Command("GET", testHash).Expect("")
	cmd := mockConn.Command("SET", testHash, testId).Expect("ok")
	mockConn.Command("EXPIRE", testHash, TenMinutes).Expect("ok")

	mockRedisConnector := NewMockRedisConnector(mockConn)
	adapted := Adapt(
		func(w http.ResponseWriter, r *http.Request) {},
		Auth(mockRedisConnector, &MockSessionManager{}),
	)

	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set("Authorization", testAuthToken)

	w := httptest.NewRecorder()
	adapted(w, req)

	c.Check(req.Header.Get(UserIdHeader), gc.Equals, testId)
	c.Check(mockConn.Stats(cmd), gc.Equals, 1)
}
