package config

import (
	"strconv"
	"time"

	"github.com/motain/samodelkin/fsloader"
	"github.com/motain/samodelkin/redis"
	"github.com/motain/samodelkin/revision"
)

// Config is a struct type which holds
// specific app configurarion values
type (
	Config struct {
		Revision    revision.AppRevision
		Port        int          `json:port`
		LogRequest  bool         `json:"log_request"`
		RedisConfig *RedisConfig `json:"redis"`
		Database    *Database    `json:"database"`
	}

	Database struct {
		Name     string `json:"name"`
		User     string `json:"user"`
		Password string `json:"password"`
		Host     string `json:"host"`
		Port     int    `json:"port"`
	}

	// RedisConfig is a struct type which describes a redis config entry
	RedisConfig struct {
		MaxIdleConns   int           `json:"max_idle_conns"`
		MaxActiveConns int           `json:"max_active_conns"`
		IdleTimeout    time.Duration `json:"idle_timeout_sec"`
		ConnectTimeout time.Duration `json:"connect_timeout_sec"`
		ReadTimeout    time.Duration `json:"read_timeout_sec"`
		WriteTimeout   time.Duration `json:"write_timeout_sec"`
		Host           string
		Password       string
		Port           int
	}
)

// NewConfig loads json config values into a package var
func NewConfig(configFile string) (*Config, error) {
	c := &Config{}
	cfgloader := fsloader.NewJSONConfigLoader()
	if err := cfgloader.Load(configFile, c); err != nil {
		return nil, err
	}

	if err := c.setDefaults(); err != nil {
		return nil, err
	}

	return c, nil
}

// setDefaults sets default config values
func (cfg *Config) setDefaults() error {
	setters := []func() error{
		cfg.Revision.Load,
	}

	for _, setter := range setters {
		if err := setter(); err != nil {
			return err
		}
	}

	return nil
}

// GetRedis returns the RedisConnector instance that is used to retrieve connections with redis
func (cfg *Config) GetRedis() (*redis.RedisConnector, error) {
	return redis.NewRedisConnector([]redis.RedisConfig{
		redis.RedisConfig{
			Host:           cfg.RedisConfig.Host,
			Password:       cfg.RedisConfig.Password,
			Port:           cfg.RedisConfig.Port,
			MaxIdleConns:   cfg.RedisConfig.MaxIdleConns,
			MaxActiveConns: cfg.RedisConfig.MaxActiveConns,
			IdleTimeout:    cfg.RedisConfig.IdleTimeout * time.Second,
			ConnectTimeout: cfg.RedisConfig.ConnectTimeout * time.Second,
			ReadTimeout:    cfg.RedisConfig.ReadTimeout * time.Second,
			WriteTimeout:   cfg.RedisConfig.WriteTimeout * time.Second,
		},
	})
}

// String return the database connection string eg. user:password@tcp(localhost:5555)/dbname
func (db *Database) String() string {
	return db.User + ":" +
		db.Password + "@tcp(" +
		db.Host + ":" +
		strconv.Itoa(db.Port) + ")/" +
		db.Name +
		"?parseTime=true&charset=utf8"
}
