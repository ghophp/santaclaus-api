package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/nbari/violetear"

	"bitbucket.org/ghophp/santaclaus-api/adapter"
	"bitbucket.org/ghophp/santaclaus-api/config"
	"bitbucket.org/ghophp/santaclaus-api/db"
	"bitbucket.org/ghophp/santaclaus-api/handler"
	"bitbucket.org/ghophp/santaclaus-api/session"
)

const DefaultConfig = "config.json"

func main() {
	cfg, err := config.NewConfig(DefaultConfig)
	if err != nil {
		panic(err)
	}

	redisConnector, err := cfg.GetRedis()
	if err != nil {
		panic(err)
	}

	db, err := db.NewDb(cfg.Database.String())
	if err != nil {
		panic(err)
	}

	paramReader := &handler.VioletearParamReader{}
	sessionManager := &session.FacebookSessionManager{}

	var (
		userHandler = handler.NewUserHandler(paramReader, redisConnector, db)
		itemHandler = handler.NewItemHandler(paramReader, redisConnector, db)
		giftHandler = handler.NewGiftHandler(paramReader, redisConnector, db)
	)

	router := violetear.New()
	router.LogRequests = cfg.LogRequest

	router.AddRegex(":item_id", `[0-9]+`)
	router.AddRegex(":receiver_id", `[0-9]+`)
	router.AddRegex(":sender_id", `[0-9]+`)

	router.HandleFunc(
		"/v1/me/register",
		adapter.Adapt(userHandler.Register, adapter.Cors()),
		"POST,OPTIONS")

	router.HandleFunc(
		"/v1/me/items",
		adapter.Adapt(userHandler.Items, adapter.Auth(redisConnector, sessionManager), adapter.Cors()),
		"GET,HEAD,OPTIONS")

	router.HandleFunc(
		"/v1/me/gifts",
		adapter.Adapt(userHandler.Gifts, adapter.Auth(redisConnector, sessionManager), adapter.Cors()),
		"GET,HEAD,OPTIONS")

	router.HandleFunc(
		"/v1/me/gift/:item_id/claim/:sender_id",
		adapter.Adapt(giftHandler.ClaimGift, adapter.Auth(redisConnector, sessionManager), adapter.Cors()),
		"POST,OPTIONS")

	router.HandleFunc(
		"/v1/me/item/:item_id/send/:receiver_id",
		adapter.Adapt(itemHandler.SendItem, adapter.Auth(redisConnector, sessionManager), adapter.Cors()),
		"POST,OPTIONS")

	var port int = cfg.Port
	if envPort := os.Getenv("PORT"); envPort != "" {
		if p, err := strconv.Atoi(envPort); err == nil {
			port = p
		}
	}

	http.ListenAndServe(fmt.Sprintf(":%d", port), router)
}
