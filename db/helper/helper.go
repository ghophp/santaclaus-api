// Package helper contains a set of functions designed to help the usage of database
// in the related packages of the database package
package helper

import (
	"database/sql/driver"
	"time"
)

// NullTime is a struct that enable developers to use optional timestamp values
// the column can be NULL for TIMESTAMP
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}
