// Package user handles the model and auxiliar functions for the user table
// and some interations with the user_item table
package user

import (
	"time"

	"bitbucket.org/ghophp/santaclaus-api/db/helper"

	"github.com/motain/gorp"
)

// User is the struct that maps the table user
type User struct {
	Id         int64           `db:"id"`
	Name       string          `db:"name"`
	CreatedAt  time.Time       `db:"created_at"`
	LastGiftAt helper.NullTime `db:"last_gift_at"`
}

// UserItem is the struct that maps the table user_item
// basically the relation N to N between users and items
type UserItem struct {
	UserId   int64 `db:"user_id"`
	ItemId   int64 `db:"item_id"`
	Quantity int64 `db:"quantity"`
}

// GetUserItemById return the item of the user (UserItem struct) if the user has it
func GetUserItemById(db gorp.SqlExecutor, userId, itemId int64) (*UserItem, error) {
	var (
		query    = "SELECT user_id, item_id, quantity FROM `user_item` WHERE user_id = :user_id AND item_id = :item_id LIMIT 1"
		params   = map[string]interface{}{"user_id": userId, "item_id": itemId}
		userItem *UserItem
	)

	if err := db.SelectOne(&userItem, query, params); err != nil {
		return nil, err
	}

	return userItem, nil
}

// GetById returns the user if it exists
func GetById(db gorp.SqlExecutor, id int) (*User, error) {
	var (
		query       = "SELECT id, name, created_at, last_gift_at FROM `user` WHERE id = ? LIMIT 1"
		user  *User = &User{}
	)

	if err := db.SelectOne(user, query, id); err != nil {
		return nil, err
	}

	return user, nil
}

// Register insert the user in the database
func Register(db gorp.SqlExecutor, id int, name string) (*User, error) {
	u := &User{
		Id:        int64(id),
		Name:      name,
		CreatedAt: time.Now(),
	}

	err := db.Insert(u)
	if err != nil {
		return u, err
	}

	return u, nil
}

// TransferItem move a UserItem quantity a new user, if the user_item rows exists it updates it,
// if not it inserts a new row with the quantity
func TransferItem(db gorp.SqlExecutor, userItem *UserItem, receiverId int64, quantity int) error {
	dbMap := db.(*gorp.DbMap)
	trans, err := dbMap.Begin()
	if err != nil {
		return err
	}

	//decrease quantity from sender
	userItem.Quantity = userItem.Quantity - int64(quantity)
	trans.Update(userItem)

	receiverItem, err := GetUserItemById(db, receiverId, userItem.ItemId)
	if err == nil {
		receiverItem.Quantity = receiverItem.Quantity + int64(quantity)
		trans.Update(receiverItem)
	} else {
		trans.Insert(&UserItem{
			UserId:   receiverId,
			ItemId:   userItem.ItemId,
			Quantity: int64(quantity),
		})
	}

	return trans.Commit()
}

// HasSentGiftsInTheLast24Hours check if the user has sent a gift in the last 24 hours
func HasSentGiftsInTheLast24Hours(user *User) bool {
	lastGift, _ := user.LastGiftAt.Value()
	if user.LastGiftAt.Valid && lastGift != nil {
		now := time.Now()
		last := lastGift.(time.Time)

		compare := time.Date(
			now.Year(),
			now.Month(),
			now.Day(),
			now.Hour(),
			now.Minute(),
			now.Second(),
			0,
			last.Location())

		delta := compare.Sub(last)
		return delta.Hours() <= 24
	}

	return false
}
