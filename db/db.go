// Package db handles the database management and mapping, through the
// structs on each subpackage and management funcs we interact with the
// database and perform the queries
package db

import (
	"database/sql"

	"bitbucket.org/ghophp/santaclaus-api/db/item"
	"bitbucket.org/ghophp/santaclaus-api/db/user"

	_ "github.com/go-sql-driver/mysql"
	"github.com/motain/gorp"
)

// NewDb return a new instance of gorp.DbMap or an error
// if was not possible to open the connection or there
// was some problem mapping
func NewDb(connection string) (*gorp.DbMap, error) {
	db, err := sql.Open("mysql", connection)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	dbMap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	dbMap.AddTableWithName(user.User{}, "user").SetKeys(false, "id")
	dbMap.AddTableWithName(item.Item{}, "item").SetKeys(true, "id")
	dbMap.AddTableWithName(user.UserItem{}, "user_item").SetKeys(false, "UserId", "ItemId")

	return dbMap, nil
}
