// Package item handles the model and auxiliar functions for the item table
// and some interations with the user_item table
package item

import (
	"encoding/json"
	"strings"
	"time"

	"bitbucket.org/ghophp/santaclaus-api/db/helper"

	redigo "github.com/garyburd/redigo/redis"
	"github.com/motain/gorp"
	"github.com/motain/samodelkin/redis"
)

const WeekInSeconds = 604800

// Item is the struct that maps the table item
type Item struct {
	Id        int64           `db:"id" json:"id"`
	Name      string          `db:"name" json:"name"`
	Picture   string          `db:"picture" json:"picture"`
	CreatedAt time.Time       `db:"created_at" json:"-"`
	UpdatedAt helper.NullTime `db:"updated_at" json:"-"`
}

type UserItem struct {
	Item
	Quantity int `db:"quantity" json:"quantity"`
}

type Gift struct {
	Item
	Sender   int64 `json:"sender_id"`
	Quantity int   `json:"quantity"`
}

// GetItemById search in the database by id and return a Item instance
func GetItemById(db gorp.SqlExecutor, id int) (*Item, error) {
	var (
		item  *Item
		query = `SELECT i.id, i.name, i.picture, i.created_at, i.updated_at
			FROM item AS i 
			WHERE i.id = ?`
	)

	if err := db.SelectOne(&item, query, id); err != nil {
		return item, err
	}

	return item, nil
}

// GetItemsByIds search in the database for multiple items ids
func GetItemsByIds(db gorp.SqlExecutor, ids []string) ([]*Item, error) {
	var (
		items []*Item
		query = `SELECT i.id, i.name, i.picture, i.created_at, i.updated_at
			FROM item AS i 
			WHERE i.id IN (` + strings.Join(ids, ",") + `)`
	)

	if _, err := db.Select(&items, query); err != nil {
		return items, err
	}

	return items, nil
}

// GetItemsByUserId joins the user_item table and just return items that are valid for the user,
// the Item struct is wrapper into UserItem in this case, to include the quantity
func GetItemsByUserId(db gorp.SqlExecutor, id int64, filterIds []string) ([]*UserItem, error) {
	var (
		items []*UserItem
		query = `SELECT i.id, i.name, i.picture, i.created_at, i.updated_at, ui.quantity 
			FROM user_item AS ui 
			INNER JOIN item AS i ON ui.item_id = i.id 
			WHERE ui.user_id = ? AND ui.quantity > 0`
	)

	if len(filterIds) > 0 {
		query = query + ` AND i.id IN (` + strings.Join(filterIds, ",") + `)`
	}

	if _, err := db.Select(&items, query, id); err != nil {
		return items, err
	}

	return items, nil
}

// GetGiftsToClaim returns the items that are available to be claimed into redis, all the pendent
// operations that are considered pendent gifts to be claimed
func GetGiftsToClaim(redisConnector redis.Connector, userId string) ([]*Gift, error) {
	var gifts = []*Gift{}

	now := time.Now().UTC()
	week := now.Add(-(time.Second * WeekInSeconds))

	rConn := redisConnector.Connect()
	defer rConn.Close()

	serializedGifts, err := redigo.Strings(rConn.Do("ZRANGEBYSCORE", userId, week.UnixNano(), now.UnixNano()))
	if err != nil {
		return gifts, err
	}

	for _, g := range serializedGifts {
		gift := &Gift{}
		err = json.Unmarshal([]byte(g), gift)
		if err != nil {
			continue
		}

		gifts = append(gifts, gift)
	}

	return gifts, nil
}
