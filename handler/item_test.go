package handler

import (
	"database/sql"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	"bitbucket.org/ghophp/santaclaus-api/adapter"
	"bitbucket.org/ghophp/santaclaus-api/db/helper"
	"bitbucket.org/ghophp/santaclaus-api/db/item"
	"bitbucket.org/ghophp/santaclaus-api/db/user"

	"github.com/motain/samodelkin/mock/gorpmock"
	"github.com/rafaeljusto/redigomock"
	gc "gopkg.in/check.v1"
)

func (h *HandlerSuite) TestGetValidAuthenticatedUserNotExist(c *gc.C) {
	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			return sql.ErrNoRows
		},
	}

	handler := NewItemHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set(adapter.UserIdHeader, "1")
	_, err = handler.getValidAuthenticatedUser(req, 0)
	c.Check(err, gc.NotNil)
}

func (h *HandlerSuite) TestGetValidAuthenticatedUserEqualsReceiver(c *gc.C) {
	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			c.Check(args, gc.HasLen, 1)
			*holder.(*user.User) = *&user.User{
				Id:         1,
				Name:       "Test",
				CreatedAt:  time.Now(),
				LastGiftAt: helper.NullTime{},
			}
			return nil
		},
	}

	handler := NewItemHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set(adapter.UserIdHeader, "1")
	_, err = handler.getValidAuthenticatedUser(req, 1)
	c.Check(err, gc.NotNil)
	c.Check(err.Error(), gc.Equals, ErrGiftYourself)
}

func (h *HandlerSuite) TestGetValidAuthenticatedUserHasSentGift24Hours(c *gc.C) {
	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			c.Check(args, gc.HasLen, 1)
			lastGift := helper.NullTime{}
			lastGift.Scan(time.Now())
			lastGift.Valid = true

			*holder.(*user.User) = *&user.User{
				Id:         1,
				Name:       "Test",
				CreatedAt:  time.Now(),
				LastGiftAt: lastGift,
			}
			return nil
		},
	}

	handler := NewItemHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set(adapter.UserIdHeader, "1")
	_, err = handler.getValidAuthenticatedUser(req, 2)
	c.Check(err, gc.NotNil)
	c.Check(err.Error(), gc.Equals, Err24HoursGift)
}

func (h *HandlerSuite) TestGetSendItemBody(c *gc.C) {
	req, err := http.NewRequest("GET", "", strings.NewReader(``))
	c.Check(err, gc.IsNil)

	handler := NewItemHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		&gorpmock.DBMapMock{},
	)

	_, err = handler.getSendItemBody(req.Body)
	c.Check(err, gc.NotNil)

	req, err = http.NewRequest("GET", "", strings.NewReader(`{"quantity": 1}`))
	c.Check(err, gc.IsNil)

	payload, err := handler.getSendItemBody(req.Body)
	c.Check(err, gc.IsNil)
	c.Check(payload.Quantity, gc.Equals, 1)
}

func (h *HandlerSuite) TestGiftAlreadyExists(c *gc.C) {
	testOperation := "test"

	mockConn := redigomock.NewConn()
	mockConn.Command("EXISTS", testOperation).Expect([]byte("true"))

	handler := NewItemHandler(
		&MockParamReader{},
		NewMockRedisConnector(mockConn),
		&gorpmock.DBMapMock{},
	)

	exists, err := handler.giftAlreadyExists(mockConn, testOperation)
	c.Check(err, gc.NotNil)
	c.Check(err.Error(), gc.Equals, ErrGiftAlreadySent)
	c.Check(exists, gc.Equals, true)
}

func (h *HandlerSuite) TestGetItemByUserId(c *gc.C) {
	expectedResult := []*item.UserItem{
		&item.UserItem{
			Item: item.Item{
				Id:        1,
				Name:      "Test Item 1",
				Picture:   "http://test.com/t.jpg",
				CreatedAt: time.Now(),
			},
		},
	}

	dbmap := &gorpmock.DBMapMock{
		SelectFunc: func(holder interface{}, query string, args []interface{}) ([]interface{}, error) {
			c.Check(holder, gc.FitsTypeOf, &[]*item.UserItem{})
			c.Check(args, gc.HasLen, 1)

			*holder.(*[]*item.UserItem) = expectedResult
			return nil, nil
		},
	}

	handler := NewItemHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	_, err := handler.getItemByUserId(1, 1, 2)
	c.Check(err, gc.NotNil)
	c.Check(err.Error(), gc.Equals, ErrNotEnoughQuantity)
}

func (h *HandlerSuite) TestSendItem(c *gc.C) {
	expectedResult := []*item.UserItem{
		&item.UserItem{
			Item: item.Item{
				Id:        1,
				Name:      "Test Item 1",
				Picture:   "http://test.com/t.jpg",
				CreatedAt: time.Now(),
			},
			Quantity: 2,
		},
	}

	testOp := GetOperationId("1", "2", "1")

	mockConn := redigomock.NewConn()
	mockConn.Command("EXISTS", testOp).Expect([]byte("false"))

	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			c.Check(args, gc.HasLen, 1)
			*holder.(*user.User) = *&user.User{
				Id:         1,
				Name:       "Test",
				CreatedAt:  time.Now(),
				LastGiftAt: helper.NullTime{},
			}
			return nil
		},
		SelectFunc: func(holder interface{}, query string, args []interface{}) ([]interface{}, error) {
			c.Check(holder, gc.FitsTypeOf, &[]*item.UserItem{})
			c.Check(args, gc.HasLen, 1)
			*holder.(*[]*item.UserItem) = expectedResult
			return nil, nil
		},
		UpdateFunc: func(list ...interface{}) (int64, error) {
			return 1, nil
		},
	}

	handler := NewItemHandler(
		&MockParamReader{},
		NewMockRedisConnector(mockConn),
		dbmap,
	)

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/test?item_id=1&receiver_id=2", strings.NewReader(`{"quantity": 1}`))
	c.Check(err, gc.IsNil)

	req.Header.Set(adapter.UserIdHeader, "1")

	handler.SendItem(w, req)
	c.Check(w.Code, gc.Equals, http.StatusOK)
}
