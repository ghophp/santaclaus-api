package handler

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"bitbucket.org/ghophp/santaclaus-api/adapter"
	"bitbucket.org/ghophp/santaclaus-api/db/helper"
	"bitbucket.org/ghophp/santaclaus-api/db/user"

	"github.com/garyburd/redigo/redis"
	"github.com/motain/samodelkin/mock/gorpmock"
	"github.com/nbari/violetear"
	gc "gopkg.in/check.v1"
)

var _ = gc.Suite(&HandlerSuite{})

type MockParamReader struct{}

func (r *MockParamReader) GetParam(w http.ResponseWriter, req *http.Request, key string) string {
	return req.URL.Query().Get(key)
}

type MockRedisConnector struct {
	rConn redis.Conn
}

func NewMockRedisConnector(rConn redis.Conn) *MockRedisConnector {
	return &MockRedisConnector{rConn}
}

func (r *MockRedisConnector) Connect() redis.Conn {
	return r.rConn
}

func (r *MockRedisConnector) PingConnect() (redis.Conn, error) {
	return r.rConn, nil
}

type HandlerSuite struct{}

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { gc.TestingT(t) }

func (h *HandlerSuite) TestParamReaderFromVioletearShouldReturnValue(c *gc.C) {
	writer := violetear.NewResponseWriter(httptest.NewRecorder())
	writer.Set(":product_id", "1111")

	req, err := http.NewRequest("GET", "/v1/products/1111", nil)
	if err != nil {
		c.Error(err)
	}

	reader := &VioletearParamReader{}
	v := reader.GetParam(writer, req, "product_id")

	c.Check(v, gc.Equals, "1111")

	v2 := reader.GetParam(writer, req, "not_found")
	c.Check(v2, gc.Equals, "")
}

func (h *HandlerSuite) TestGetOperationId(c *gc.C) {
	sId := "test"
	rId := "test"
	iId := "test"

	ck := fmt.Sprintf("%x", md5.Sum([]byte(sId+"-"+rId+"-"+iId)))
	c.Check(ck, gc.Equals, GetOperationId(sId, rId, iId))
}

func (h *HandlerSuite) TestGetUserAuthenticated(c *gc.C) {
	expectedQuery := "SELECT id, name, created_at, last_gift_at FROM `user` WHERE id = ? LIMIT 1"
	userId := "1"

	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	_, err = GetUserAuthenticated(req, nil)
	c.Check(err, gc.NotNil)

	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			c.Check(holder, gc.FitsTypeOf, &user.User{})
			c.Check(query, gc.Equals, expectedQuery)
			c.Check(args, gc.HasLen, 1)

			*holder.(*user.User) = *&user.User{
				Id:         1,
				Name:       "Test",
				CreatedAt:  time.Now(),
				LastGiftAt: helper.NullTime{},
			}

			return nil
		},
	}

	req.Header.Set(adapter.UserIdHeader, userId)
	u, err := GetUserAuthenticated(req, dbmap)

	c.Check(err, gc.IsNil)
	c.Check(u.Id, gc.Equals, int64(1))
}
