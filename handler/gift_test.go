package handler

import (
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"time"

	"bitbucket.org/ghophp/santaclaus-api/adapter"
	"bitbucket.org/ghophp/santaclaus-api/db/helper"
	"bitbucket.org/ghophp/santaclaus-api/db/item"
	"bitbucket.org/ghophp/santaclaus-api/db/user"

	"github.com/motain/samodelkin/mock/gorpmock"
	"github.com/rafaeljusto/redigomock"
	gc "gopkg.in/check.v1"
)

func (h *HandlerSuite) TestClaimGiftInvalidParams(c *gc.C) {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	handler := NewGiftHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		&gorpmock.DBMapMock{},
	)

	handler.ClaimGift(w, req)
	c.Check(w.Code, gc.Equals, http.StatusBadRequest)

	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			return sql.ErrNoRows
		},
	}

	req, err = http.NewRequest("GET", "/test?item_id=1&sender_id=1", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set(adapter.UserIdHeader, "1")
	handler = NewGiftHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	w = httptest.NewRecorder()
	handler.ClaimGift(w, req)
	c.Check(w.Code, gc.Equals, http.StatusInternalServerError)
}

func (h *HandlerSuite) TestClaimGiftNotFound(c *gc.C) {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/test?item_id=1&sender_id=1", nil)
	c.Check(err, gc.IsNil)

	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			c.Check(holder, gc.FitsTypeOf, &user.User{})
			c.Check(args, gc.HasLen, 1)

			*holder.(*user.User) = *&user.User{
				Id:         1,
				Name:       "Test",
				CreatedAt:  time.Now(),
				LastGiftAt: helper.NullTime{},
			}

			return nil
		},
	}

	req.Header.Set(adapter.UserIdHeader, "1")
	handler := NewGiftHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	handler.ClaimGift(w, req)
	c.Check(w.Code, gc.Equals, http.StatusGone)
}

func (h *HandlerSuite) TestClaimGiftQuantityInsuficient(c *gc.C) {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "/test?item_id=1&sender_id=1", nil)
	c.Check(err, gc.IsNil)

	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			switch holder.(type) {
			default:
				return errors.New("unexpected type")
			case *user.User:
				c.Check(args, gc.HasLen, 1)

				*holder.(*user.User) = *&user.User{
					Id:         1,
					Name:       "Test",
					CreatedAt:  time.Now(),
					LastGiftAt: helper.NullTime{},
				}
				return nil
			case *user.UserItem:
				c.Check(args, gc.HasLen, 2)

				*holder.(*user.UserItem) = *&user.UserItem{
					UserId:   1,
					ItemId:   1,
					Quantity: 1,
				}
				return nil
			}
			return nil
		},
	}

	operationId := GetOperationId("1", "1", "1")
	mockGift := &item.Gift{
		Item: item.Item{
			Id:        1,
			Name:      "Test Item 1",
			Picture:   "http://test.com/t.jpg",
			CreatedAt: time.Now(),
		},
		Sender:   1,
		Quantity: 2,
	}

	serializedGift, err := json.Marshal(mockGift)
	c.Check(err, gc.IsNil)

	mockConn := redigomock.NewConn()
	mockConn.Command("GET", operationId).Expect(string(serializedGift))

	req.Header.Set(adapter.UserIdHeader, "1")
	handler := NewGiftHandler(
		&MockParamReader{},
		NewMockRedisConnector(mockConn),
		dbmap,
	)

	handler.ClaimGift(w, req)
	c.Check(w.Code, gc.Equals, http.StatusGone)
}
