package handler

import (
	"database/sql"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"time"

	"bitbucket.org/ghophp/santaclaus-api/adapter"
	"bitbucket.org/ghophp/santaclaus-api/db/helper"
	"bitbucket.org/ghophp/santaclaus-api/db/item"
	"bitbucket.org/ghophp/santaclaus-api/db/user"

	"github.com/motain/samodelkin/mock/gorpmock"
	"github.com/rafaeljusto/redigomock"

	gc "gopkg.in/check.v1"
)

func (h *HandlerSuite) TestRegisterInvalidPayload(c *gc.C) {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "", strings.NewReader(""))
	c.Check(err, gc.IsNil)

	handler := NewUserHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		&gorpmock.DBMapMock{},
	)

	handler.Register(w, req)
	c.Check(w.Code, gc.Equals, http.StatusBadRequest)

	req, err = http.NewRequest("POST", "", strings.NewReader(`{"id": 1, "name": ""}`))
	c.Check(err, gc.IsNil)

	w = httptest.NewRecorder()
	handler.Register(w, req)
	c.Check(w.Code, gc.Equals, http.StatusBadRequest)
}

func (h *HandlerSuite) TestRegisterSuccessFoundUser(c *gc.C) {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "", strings.NewReader(`{"id": 1, "name": "Test"}`))
	c.Check(err, gc.IsNil)

	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			c.Check(holder, gc.FitsTypeOf, &user.User{})
			c.Check(args, gc.HasLen, 1)

			*holder.(*user.User) = *&user.User{
				Id:         1,
				Name:       "Test",
				CreatedAt:  time.Now(),
				LastGiftAt: helper.NullTime{},
			}

			return nil
		},
	}

	handler := NewUserHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	handler.Register(w, req)
	c.Check(w.Code, gc.Equals, http.StatusOK)
}

func (h *HandlerSuite) TestRegisterSuccessInsertUser(c *gc.C) {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("POST", "", strings.NewReader(`{"id": 1, "name": "Test"}`))
	c.Check(err, gc.IsNil)

	dbmap := &gorpmock.DBMapMock{
		SelectOneFunc: func(holder interface{}, query string, args []interface{}) error {
			return sql.ErrNoRows
		},
		InsertFunc: func(list ...interface{}) error {
			return nil
		},
	}

	handler := NewUserHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	handler.Register(w, req)
	c.Check(w.Code, gc.Equals, http.StatusOK)
}

func (h *HandlerSuite) TestItemsGiftsNoUserError(c *gc.C) {
	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	handler := NewUserHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		&gorpmock.DBMapMock{},
	)

	handler.Items(w, req)
	c.Check(w.Code, gc.Equals, http.StatusInternalServerError)

	w = httptest.NewRecorder()
	handler.Gifts(w, req)
	c.Check(w.Code, gc.Equals, http.StatusInternalServerError)
}

func (h *HandlerSuite) TestItems(c *gc.C) {
	now := time.Now()
	expectedResult := []*item.UserItem{
		&item.UserItem{
			Item: item.Item{
				Id:        1,
				Name:      "Test Item 1",
				Picture:   "http://test.com/t.jpg",
				CreatedAt: now,
			},
		},
	}

	expectedBody, err := json.Marshal(expectedResult)
	c.Check(err, gc.IsNil)

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set(adapter.UserIdHeader, "1")

	dbmap := &gorpmock.DBMapMock{
		SelectFunc: func(holder interface{}, query string, args []interface{}) ([]interface{}, error) {
			c.Check(holder, gc.FitsTypeOf, &[]*item.UserItem{})
			c.Check(args, gc.HasLen, 1)

			*holder.(*[]*item.UserItem) = expectedResult
			return nil, nil
		},
	}

	handler := NewUserHandler(
		&MockParamReader{},
		NewMockRedisConnector(redigomock.NewConn()),
		dbmap,
	)

	handler.Items(w, req)
	c.Check(w.Code, gc.Equals, http.StatusOK)
	c.Check(w.Body.String(), gc.Equals, string(expectedBody))
}

func (h *HandlerSuite) TestGifts(c *gc.C) {
	now := time.Now()
	expectedResult := []*item.Gift{
		&item.Gift{
			Item: item.Item{
				Id:        1,
				Name:      "Test Item 1",
				Picture:   "http://test.com/t.jpg",
				CreatedAt: now,
			},
			Sender:   1,
			Quantity: 1,
		},
	}

	expectedBody, err := json.Marshal(expectedResult)
	c.Check(err, gc.IsNil)

	serializedGift, err := json.Marshal(expectedResult[0])
	c.Check(err, gc.IsNil)

	w := httptest.NewRecorder()
	req, err := http.NewRequest("GET", "", nil)
	c.Check(err, gc.IsNil)

	req.Header.Set(adapter.UserIdHeader, "1")

	mockConn := redigomock.NewConn()
	mockConn.GenericCommand("ZRANGEBYSCORE").ExpectMap(map[string]string{
		"1": string(serializedGift),
	})

	handler := NewUserHandler(
		&MockParamReader{},
		NewMockRedisConnector(mockConn),
		&gorpmock.DBMapMock{},
	)

	handler.Gifts(w, req)
	c.Check(w.Code, gc.Equals, http.StatusOK)
	c.Check(w.Body.String(), gc.Equals, string(expectedBody))
}
