// Package handler is a structural package to separate the handlers
package handler

import (
	"crypto/md5"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/nbari/violetear"

	"github.com/motain/gorp"

	"bitbucket.org/ghophp/santaclaus-api/adapter"
	"bitbucket.org/ghophp/santaclaus-api/db/user"
)

const (
	ContentType = "Content-Type"
	JSONType    = "application/json"
)

// ParamReader defines how a parameter reader should be implemented, this is used
// to get parameters from the URL and allow mocking this process in the tests
type ParamReader interface {
	// GetParam returns the value for the key, can use both the response writter or the request
	// to perform this operation, one of the implementations is VioletearParamReader that uses
	// the violetear.ResponseWriter to get the route params
	GetParam(w http.ResponseWriter, req *http.Request, key string) string
}

// VioletearParamReader implements the ParamReader interface, it is focused on using the
// violetear router to get the parameters from the URL, for example /v1/products/:product_id
type VioletearParamReader struct{}

func (r *VioletearParamReader) GetParam(w http.ResponseWriter, req *http.Request, key string) string {
	if cw, ok := w.(*violetear.ResponseWriter); ok {
		if v := cw.Get(":" + key); v != nil {
			return v.(string)
		}
	}
	return ""
}

type ErrorPayload struct {
	Message string `json:"message"`
}

func SendError(w http.ResponseWriter, err string, code int) {
	errorPayload := &ErrorPayload{
		Message: err,
	}

	errorSerialized, _ := json.Marshal(errorPayload)

	w.WriteHeader(code)
	w.Header().Set(ContentType, JSONType)
	w.Write(errorSerialized)
}

// GetOperationId hashes in md5 the sender id, receiver id and item id
// conceptually MD5(senderid-receiverid-itemid)
func GetOperationId(senderId, receiverId, itemId string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(senderId+"-"+receiverId+"-"+itemId)))
}

// GetUserAuthenticated returns the User entity, using the preset id on the request header
func GetUserAuthenticated(r *http.Request, db gorp.SqlExecutor) (*user.User, error) {
	userId, err := strconv.Atoi(r.Header.Get(adapter.UserIdHeader))
	if err != nil {
		return nil, err
	}

	u, err := user.GetById(db, userId)
	if err != nil {
		return nil, err
	}

	return u, nil
}
