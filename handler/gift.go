package handler

import (
	"encoding/json"
	"net/http"
	"strconv"

	"bitbucket.org/ghophp/santaclaus-api/db/item"
	"bitbucket.org/ghophp/santaclaus-api/db/user"

	redigo "github.com/garyburd/redigo/redis"
	"github.com/motain/gorp"

	"github.com/motain/samodelkin/redis"
)

const (
	ErrInvalidClaimGiftParams = "item_id or receiver_id not informed"
	ErrGiftNotExist           = "the gift that you are claiming doesn't exists"
	ErrNoMoreItem             = "the recepient doesn't have anymore this item"
)

type GiftHandler struct {
	ParamReader    ParamReader
	RedisConnector redis.Connector
	Db             gorp.SqlExecutor
}

// NewGiftHandler return a instance of a GiftHandler
func NewGiftHandler(ParamReader ParamReader, RedisConnector redis.Connector, Db gorp.SqlExecutor) *GiftHandler {
	return &GiftHandler{ParamReader, RedisConnector, Db}
}

// ClaimGift receive data from the url params and based on the user on the session
// it checks if the operation is still possible to be completed, for example check if the user
// still has the amount of items to gift, and then if all is fine with criterias, perform
// the transfer of items from one user to another and erase the keys that control the operations
func (h *GiftHandler) ClaimGift(w http.ResponseWriter, r *http.Request) {
	itemId, _ := strconv.Atoi(h.ParamReader.GetParam(w, r, "item_id"))
	senderId, _ := strconv.Atoi(h.ParamReader.GetParam(w, r, "sender_id"))
	if itemId <= 0 || senderId <= 0 {
		SendError(w, ErrInvalidClaimGiftParams, http.StatusBadRequest)
		return
	}

	u, err := GetUserAuthenticated(r, h.Db)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	operationId := GetOperationId(
		strconv.Itoa(senderId),
		strconv.FormatInt(u.Id, 10),
		strconv.Itoa(itemId))

	rConn := h.RedisConnector.Connect()
	defer rConn.Close()

	value, err := redigo.String(rConn.Do("GET", operationId))
	if err != nil || len(value) <= 0 {
		SendError(w, ErrGiftNotExist, http.StatusGone)
		return
	}

	var gift item.Gift
	err = json.Unmarshal([]byte(value), &gift)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	rConn.Send("DEL", operationId)
	rConn.Send("ZREM", u.Id, value)

	item, err := user.GetUserItemById(h.Db, int64(senderId), gift.Item.Id)
	if err != nil || item.Quantity < int64(gift.Quantity) {
		// if the sender dont have more the item or the quantity of the item
		// just erase the keys and return that the item was claimed already
		rConn.Flush()

		SendError(w, ErrNoMoreItem, http.StatusGone)
		return
	}

	err = user.TransferItem(h.Db, item, u.Id, gift.Quantity)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	rConn.Flush()

	giftResult, err := json.Marshal(gift)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set(ContentType, JSONType)
	w.Write(giftResult)
}
