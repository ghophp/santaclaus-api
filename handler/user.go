package handler

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/motain/gorp"

	"bitbucket.org/ghophp/santaclaus-api/adapter"
	"bitbucket.org/ghophp/santaclaus-api/db/item"
	"bitbucket.org/ghophp/santaclaus-api/db/user"
	"github.com/motain/samodelkin/redis"
)

type UserHandler struct {
	ParamReader    ParamReader
	RedisConnector redis.Connector
	Db             gorp.SqlExecutor
}

type RegisterBody struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

// NewUserHandler return a instance of a UserHandler
func NewUserHandler(ParamReader ParamReader, RedisConnector redis.Connector, Db gorp.SqlExecutor) *UserHandler {
	return &UserHandler{ParamReader, RedisConnector, Db}
}

// Register receive a user id and checks if it exists or not, if not it inserts it
// into the database for track relations
func (h *UserHandler) Register(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		SendError(w, err.Error(), http.StatusBadRequest)
		return
	}

	var payload RegisterBody
	err = json.Unmarshal(body, &payload)
	if err != nil {
		SendError(w, err.Error(), http.StatusBadRequest)
		return
	}

	if payload.Id <= 0 || len(payload.Name) <= 0 {
		SendError(w, "id or name not informed", http.StatusBadRequest)
		return
	}

	_, err = user.GetById(h.Db, payload.Id)
	if err != nil {
		_, err = user.Register(h.Db, payload.Id, payload.Name)
		if err != nil {
			SendError(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	w.WriteHeader(http.StatusOK)
}

// Items return the list of items from the user on the session
func (h *UserHandler) Items(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.Atoi(r.Header.Get(adapter.UserIdHeader))
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	items, err := item.GetItemsByUserId(h.Db, int64(userId), nil)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	itemsResult, err := json.Marshal(items)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set(ContentType, JSONType)
	w.Write(itemsResult)
}

// Gifts return the list of gifts the user on the session have pendent to claim
func (h *UserHandler) Gifts(w http.ResponseWriter, r *http.Request) {
	userId, err := strconv.Atoi(r.Header.Get(adapter.UserIdHeader))
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	gifts, err := item.GetGiftsToClaim(h.RedisConnector, strconv.Itoa(userId))
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	giftsResult, err := json.Marshal(gifts)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set(ContentType, JSONType)
	w.Write(giftsResult)
}
