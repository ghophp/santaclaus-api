package handler

import (
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/motain/gorp"

	"bitbucket.org/ghophp/santaclaus-api/db/item"
	"bitbucket.org/ghophp/santaclaus-api/db/user"
	"github.com/motain/samodelkin/redis"

	redigo "github.com/garyburd/redigo/redis"
)

const (
	ErrGiftYourself      = "you cannot send gifts to yourself"
	Err24HoursGift       = "already sent gift in the last 24 hours"
	ErrGiftAlreadySent   = "gift already sent to your friend"
	ErrNotEnoughQuantity = "you don't have that much items to send"
)

type (
	ItemHandler struct {
		ParamReader    ParamReader
		RedisConnector redis.Connector
		Db             gorp.SqlExecutor
	}

	SendItemBody struct {
		Quantity int `json:"quantity"`
	}
)

// NewItemHandler return a instance of a ItemHandler
func NewItemHandler(ParamReader ParamReader, RedisConnector redis.Connector, Db gorp.SqlExecutor) *ItemHandler {
	return &ItemHandler{ParamReader, RedisConnector, Db}
}

// getValidAuthenticatedUser returns the user if basic validations for this endpoint are met:
// - user id is not the same as the receiver
// - the user has not sent a gift in the last 24 hours
func (h *ItemHandler) getValidAuthenticatedUser(r *http.Request, receiverId int) (*user.User, error) {
	u, err := GetUserAuthenticated(r, h.Db)
	if err != nil {
		return nil, err
	}

	if u.Id == int64(receiverId) {
		return nil, errors.New(ErrGiftYourself)
	}

	if user.HasSentGiftsInTheLast24Hours(u) {
		return nil, errors.New(Err24HoursGift)
	}

	return u, nil
}

// getSendItemBody parses and return the body for the given request
func (h *ItemHandler) getSendItemBody(requestBody io.ReadCloser) (*SendItemBody, error) {
	payload := &SendItemBody{}
	body, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(body, payload)
	if err != nil {
		return nil, err
	}

	return payload, nil
}

// giftAlreadyExists return true and error if the operationId already exists
func (h *ItemHandler) giftAlreadyExists(rConn redigo.Conn, operationId string) (bool, error) {
	exists, err := redigo.Bool(rConn.Do("EXISTS", operationId))
	if err != nil {
		return false, err
	}

	if exists {
		return true, errors.New(ErrGiftAlreadySent)
	}

	return false, nil
}

// getItemByUserId return an item if the quantity if valid
func (h *ItemHandler) getItemByUserId(userId int64, itemId int, quantity int) (*item.UserItem, error) {
	items, err := item.GetItemsByUserId(h.Db, userId, []string{strconv.Itoa(itemId)})
	if err != nil {
		return nil, err
	}

	if len(items) <= 0 || items[0].Quantity < quantity {
		return nil, errors.New(ErrNotEnoughQuantity)
	}

	return items[0], nil
}

// SendItem receive part of the data from the url params and the quantity from the body payload
// use this data to retrieve the user and check his items, if everything is okay a operation
// is created on redis
func (h *ItemHandler) SendItem(w http.ResponseWriter, r *http.Request) {
	itemId, _ := strconv.Atoi(h.ParamReader.GetParam(w, r, "item_id"))
	receiverId, _ := strconv.Atoi(h.ParamReader.GetParam(w, r, "receiver_id"))
	if itemId <= 0 || receiverId <= 0 {
		SendError(w, "item_id or receiver_id not informed", http.StatusBadRequest)
		return
	}

	payload, err := h.getSendItemBody(r.Body)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	u, err := h.getValidAuthenticatedUser(r, receiverId)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	operationId := GetOperationId(
		strconv.FormatInt(u.Id, 10),
		strconv.Itoa(receiverId),
		strconv.Itoa(itemId))

	rConn := h.RedisConnector.Connect()
	defer rConn.Close()

	_, err = h.giftAlreadyExists(rConn, operationId)
	if err != nil {
		SendError(w, err.Error(), http.StatusConflict)
		return
	}

	i, err := h.getItemByUserId(u.Id, itemId, payload.Quantity)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	serialized, err := json.Marshal(&item.Gift{
		Item:     i.Item,
		Sender:   u.Id,
		Quantity: payload.Quantity,
	})

	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	rConn.Send("SET", operationId, serialized)
	rConn.Send("EXPIRE", operationId, item.WeekInSeconds)
	rConn.Send("ZADD", receiverId, time.Now().UTC().UnixNano(), serialized)
	if err := rConn.Flush(); err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	u.LastGiftAt.Scan(time.Now())
	_, err = h.Db.Update(u)
	if err != nil {
		SendError(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}
