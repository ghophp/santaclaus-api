.PHONY: all deps test build clean config version

GO ?= go
COPY ?= cp
ECHO ?= echo
GOVERALLS ?= goveralls

PACKAGE_NAME=app.bz2
BIN_FOLDER=bin
BIN_NAME=santaclaus-api
REVISION_NAME=REVISION

all: build test

deps:
	git config --global url.git@github.com:motain.insteadOf https://github.com/motain
	${GO} get github.com/motain/samodelkin/fsloader
	${GO} get github.com/motain/samodelkin/revision
	${GO} get github.com/motain/samodelkin/redis
	${GO} get github.com/motain/gorp
	${GO} get github.com/go-sql-driver/mysql
	${GO} get github.com/garyburd/redigo/redis
	${GO} get github.com/nbari/violetear
	${GO} get github.com/huandu/facebook
	${GO} get gopkg.in/check.v1
	${GO} get github.com/rafaeljusto/redigomock

build: deps
build: version
build:
	${GO} build -o bin/${BIN_NAME}

	@if test -n "${CI_BRANCH}"; then \
	if test "${CI_BRANCH}" = "develop"; then \
	${COPY} config.json.staging bin/config.json; \
	else \
	${COPY} config.json.prod bin/config.json; \
	fi; \
	fi;

	${COPY} ${REVISION_NAME} bin/
	tar -jcf app.bz2 bin

test: deps
test: version
test:
	${GO} test ./... -v

version:
	${ECHO} `git log -n 1 --pretty=format:"%H"` > ${REVISION_NAME}

clean:
	@rm -rf ${BIN_FOLDER}
	@rm ${PACKAGE_NAME}