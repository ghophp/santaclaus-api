# santaclaus-api ![build status](https://codeship.com/projects/98a25760-2b5f-0134-368a-06ed2d662211/status?branch=master) [![Coverage Status](https://coveralls.io/repos/bitbucket/ghophp/santaclaus-api/badge.svg?branch=master)](https://coveralls.io/bitbucket/ghophp/santaclaus-api?branch=master) [![GoDoc](https://godoc.org/github.com/golang/gddo?status.svg)](https://godoc.org/bitbucket.org/ghophp/santaclaus-api)

Santa Claus api is the API that provide the data endpoints for the project [santa-claus-frontend](https://bitbucket.org/ghophp/santaclaus-frontend). It has a basic router based on violetear that handle the requests and forward to the proper handler. The handler interact with other packages to return json based responses.

Setup
-----

Installing Dependencies
-----------------------

`go get ./...`

Config
------

`cp config.json.dist config.json`

Running Application
-------------------

`go run main.go`

Tests
-----

Run all the tests: `go test ./...`

Build
-----

```
make
```

The result of this build will be a `app.bz2` in the project root containing a folder named `bin`, this folder contains the binary to run the project, the `REVISION` file with the commit hash, and the `config.json` file with the necessary configuration for the current environment.