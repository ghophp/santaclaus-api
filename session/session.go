package session

import (
	fb "github.com/huandu/facebook"
)

type SessionManager interface {
	GetUserDetails(token string) (string, error)
}

type FacebookSessionManager struct{}

func (m *FacebookSessionManager) GetUserDetails(token string) (string, error) {
	res, err := fb.Get("/me", fb.Params{"access_token": token})
	if err != nil {
		return "", err
	}

	return res["id"].(string), nil
}
